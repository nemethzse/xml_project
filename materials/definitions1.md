> #### _kódmágia_

# XML

```definition```, ```xml```, ```xsd```, ```dtd```

Az Extensible Markup Language, röviden XML, egy olyan általános célú jelölõnyelv, amellyel az adatok tárolása-leírása szabványossá tehetõ, elõnyei közé tartozik, hogy formátuma ember által is könnyen olvasható, értelmezhetõ. Platformfüggetlen, alkalmazástól
független adatcserét tesz lehetővé.

HTML ↔ XML: adatok megjelenítése ↔ adatok leírása

Az XML tag-ek nincsenek előre meghatározva, egyszerű szintaxis, szigorú
szabályok. Az XML állományban tárolt adat szerkezete leírható DTD (Document Type
Definition) vagy XML séma (XSD) segítségével
```xml
<?xml version="1.0" encoding="UTF-8"?>
<Personnel>
    <Employee type="full-time">
        <Name>Juliska</Name>
        <Id>1234</Id>
        <Age>23</Age>
    </Employee>
    <Employee type="part-time">
        <Name>Jancsika</Name>
        <Id>1235</Id>
        <Age>34</Age>
    </Employee>
</Personnel>
```
## 1. XML szabályai

1. Minden elemnek kell legyen záró tag-je
2. Számít a kis- vagy nagybetű
3. A tag-eket helyesen kell egymásba ágyazni (nem lehetnek egymásba
ékelve)
4. A dokumentumnak egy és csakis egy gyökér eleme lehet
5. Az attribútumok értékeit kötelező idézőjelbe (” vagy ’) tenni
6. A fehér karakterek figyelembe lesznek véve
7. Újsor: LF, Megjegyzés: <!– – XML comment – – >
8. Elemek közti viszonyok: szülő, gyerek, testvér
9. Elem felépítése: kezdő tag, törzs, záró tag (lehet üres is <tagnev . . .
/tagnev>). Az elemnek lehetnek attribútumai
10. A különböző állományokból származó azonos nevű elemek esetében a
névkonfliktusok feloldására XML névtereket alkalmazunk

## 2. XML formai leírása
Egy XML dokumentum jól formált (well formed): ha megfelel az XML
szintaktikai szabályainak

Egy XML dokumentum érvényes (valid): ha jól formált, és megfelel a
dokumentum séma definíciójának

Séma definíció: egy bizonyos DTD-ben vagy XML sémában (XSD) megadott
szabályok)

## 3. DTD
DTD (Document Type Definition): elterjedt séma-leíró módszer, amely
megadja az XML dokumentum érvényes építőelemeit (elemek,
attribútumok), illetve felépítését. Szabványos, de nem XML alapú.
```
<!DOCTYPE TVSCHEDULE [
<!ELEMENT TVSCHEDULE (CHANNEL+)>
<!ELEMENT CHANNEL (BANNER, DAY+)>
<!ELEMENT BANNER (#PCDATA)>
<!ELEMENT DAY ((DATE, HOLIDAY) | (DATE, PROGRAMSLOT+))+>
<!ELEMENT HOLIDAY (#PCDATA)>
<!ELEMENT DATE (#PCDATA)>
<!ELEMENT PROGRAMSLOT (TIME, TITLE, DESCRIPTION?)>
<!ELEMENT TIME (#PCDATA)>
<!ELEMENT TITLE (#PCDATA)>
<!ELEMENT DESCRIPTION (#PCDATA)>
<!ATTLIST TVSCHEDULE NAME CDATA #REQUIRED>
<!ATTLIST CHANNEL CHAN CDATA #REQUIRED>
<!ATTLIST PROGRAMSLOT VTR CDATA #IMPLIED>
<!ATTLIST TITLE RATING CDATA #IMPLIED>
<!ATTLIST TITLE LANGUAGE CDATA #IMPLIED>
]> 
```
__Magyarázat__ - [Wikipedia](https://hu.wikipedia.org/wiki/DTD)

## 4. XSD
XSD (XML Schema Definition): a DTD-nek XML alapú alternatívája,
meghatározza, hogy milyen elemek és attribútumok szerepelhetnek egy
dokumentumban, milyen beágyazott (gyerek) elemek vannak, és
meghatározza ezek számát, illetve előfordulásának sorrendjét

Egyszerű elem: ```<xs:element name=”xx” type=”yy”/>```
Beépített típusok: ```xs:string, xs:decimal, xs:integer, xs:boolean, xs:date, xs:time.```
Egyszerű elemnek lehet alapértelmezett (default=”. . . ” ), vagy rögzített (fixed=”. . . ”) értéke.

Attribútum: ```<xs:attribute name=”xx” type=”yy”/>```. Lehet alapértelmezett, vagy rögzített értéke. Kötelező: use="required"

Összetett elem: más beágyazott elemeket és attribútumokat tartalmazó elem

Megszorítások (facets): megadhatjuk az elemek és attribútumok elfogadható
értékeit.
```<xs:restriction base=”xs:integer”> <xs:minInclusive value=”1990”/> <xs:maxInclusive value=”2010”/>
</xs:restriction>``` Felsorolás ```(xs:enumeration value="ertek")```, reguláris kifejezés ```(xs:pattern value="[a-z]")```, hossz (```xs:length
value="…")(+minLength, maxLength)```, fehér karakterek ```(xs:whiteSpace value="preserve") (replace,
collapse)```

```
<?xml version="1.0" encoding="ISO-8859-1" ?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
<!-- definition of simple elements -->
<xs:element name="orderperson" type="xs:string"/>
<xs:element name="name" type="xs:string"/>
<xs:element name="address" type="xs:string"/>
<xs:element name="city" type="xs:string"/>
<xs:element name="country" type="xs:string"/>
<xs:element name="title" type="xs:string"/>
<xs:element name="note" type="xs:string"/>
<xs:element name="quantity" type="xs:positiveInteger"/>
<xs:element name="price" type="xs:decimal"/>
<!-- definition of attributes -->
<xs:attribute name="orderid" type="xs:string"/>
<!-- definition of complex elements -->
<xs:element name="shipto">
    <xs:complexType>
        <xs:sequence>
            <xs:element ref="name"/>
            <xs:element ref="address"/>
            <xs:element ref="city"/>
            <xs:element ref="country"/>
        </xs:sequence>
    </xs:complexType>
</xs:element>
```
[Restriction example](https://www.w3schools.com/xml/el_restriction.asp)

## 5. XML – DTD/XSD megfeleltetés
Közvetlen megfeleltetés: ```<!DOCTYPE root-element SYSTEM "dtdfile.dtd" >```, ahol
dtdfile.dtd a dtd állomány, vagy a schemaLocation attribútum használata:
```<xsi:schemaLocation = "http://www.ibm.com/schema.xsd">```, ahol schema.xsd az XML séma neve

__Extra olvasgatni való__ - [Link1](http://tudasbazis.sulinet.hu/hu/informatika/informatika/informatika-9-12-evfolyam/adatok-mentese/xml-felepitese)
[Link2](http://www.ich.hu/xml.html) [Link3](http://people.inf.elte.hu/kiss/10bir/Habilitacios%20eloadas%20%28rovid%29.ppt)

[XML Feldolgozás](definitions2.md)

> #### _kódmágia_
