> #### _kódmágia_

# XML feldolgozás

```definition```, ```xml```, ```JAXP```, ```JDOM```,```SAX```,```DOM```,

Megnézzük, hogy miként lehet feldolgozni az XML állományok tartalmát, illeve hogyan lehet létrehozni XML állományokat programkód segítségével.
## 1. DOM feldolgozás

DOM (Document Object Model) – Platform- és nyelv-független standard
XML dokumentumok feldolgozására. A dokumentumot egy fa-szerkezet
formájában ábrázolja, melynek csomópontjai az elemek, attribútumok, illetve
szövegrészek. Standard API-t biztosít az XML dokumentumok
feldolgozására. W3C standard.

![figure](https://vichargrave.github.io/img/dom.png)

## 2. SAX feldolgozás
SAX (Simple API for XML): XML dokumentumok szekvenciális feldolgozására
szolgáló API, a DOM egy igen elterjedt alternatívája

A DOM-tól eltérően nincs neki megfelelő formális specifikáció, a Java
implementációt tekintik iránymutatónak.

SAX feldolgozó (parser): egy SAX-ot implementáló feldolgozó, adatfolyam
feldolgozóként működik, eseményvezérelt API-val.

Egy-egy esemény generálódik a következő elemek feldolgozása esetén: XML
elem csomópontok, szövege tartalmazó XML csomópontok, XML feldolgozó
utasítások, XML megjegyzések

A feldolgozás egyirányú: a már feldolgozott adatot nem lehet újraolvasni (csak
ha újrakezdjük a feldolgozást)

Kevesebb memóriát igényel, mint a DOM (ahol a teljes fa-szerkezetet a
memóriában kell tárolni), gyorsabb feldolgozást tesz lehetővé, nagyméretű
dokumentumok esetében is használható

## 3. XMl feldolgozása JAVA-ban
### 3.1 XML
Adott a következő XML amit fel szeretnék dolgozni:
```
<?xml version="1.0" encoding="UTF-8"?>
<Personnel>
    <Employee type="full-time">
        <Name>Juliska</Name>
        <Id>1234</Id>
        <Age>23</Age>
        </Employee>
    <Employee type="part-time">
        <Name>Jancsika</Name>
        <Id>1235</Id>
        <Age>34</Age>
    </Employee>
</Personnel>
```
### 3.2 DOM alkalmazása
```java
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import codeismagic.java8features.model.Employee;

public class XMLDomProcessing extends DefaultHandler{
	private List<Employee> myEmployees;
	private Document dom;

	public XMLDomProcessing() {
		myEmployees = new ArrayList<Employee>();
	}

	public void runExample() {
		parseXmlFile();
		parseDocument();
		printData();
	}

	private void parseXmlFile() {
		// factory instance
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {
			// document builder instance
			DocumentBuilder db = dbf.newDocumentBuilder();
			// dom representation
			dom = db.parse("resources/employees.xml");
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	private void parseDocument() {
		// the root element
		Element docEle = dom.getDocumentElement();
		// employee node list
		NodeList nl = docEle.getElementsByTagName("Employee");
		if (nl != null && nl.getLength() > 0) {
			for (int i = 0; i < nl.getLength(); i++) {
				Element el = (Element) nl.item(i);
				Employee e = getEmployee(el);
				myEmployees.add(e);
			}
		}
	}

	/**
	 * Creating an Employee instance, using an employee element
	 */
	private Employee getEmployee(Element empEl) {
		String name = getTextValue(empEl, "Name");
		int id = getIntValue(empEl, "Id");
		int age = getIntValue(empEl, "Age");
		String type = empEl.getAttribute("type");
		Employee e = new Employee(name, id, age, type);
		return e;
	}

	/**
	 * Gets a String information from a specified text element ex.
	 * <employee><name>Jancsika</name></employee> If Element is a reference to this
	 * employee node, the value of the tagName parameter is name, the returned value
	 * will be Jancsika
	 *
	 */
	private String getTextValue(Element ele, String tagName) {
		String textVal = null;
		NodeList nl = ele.getElementsByTagName(tagName);
		if (nl != null && nl.getLength() > 0) {
			Element el = (Element) nl.item(0);
			textVal = el.getFirstChild().getNodeValue();
		}
		return textVal;
	}

	/**
	 * Calls getTextValue and converts the result
	 */
	private int getIntValue(Element ele, String tagName) {
		return Integer.parseInt(getTextValue(ele, tagName));
	}

	private void printData() {
		System.out.println("No of Employees '" + myEmployees.size() + "'.");
		Iterator<Employee> it = myEmployees.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	public static void main(String[] args) {
		XMLDomProcessing dpe = new XMLDomProcessing();
		dpe.runExample();
	}
}
```
[Employee.java](https://gitlab.com/codeismagic/materials/basesamples/blob/master/src/codeismagic/java8features/model/Employee.java)

### 3.3 SAX alkalmazása
```java
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import codeismagic.java8features.model.Employee;

public class XMLSaxProcessing extends DefaultHandler {
	private List<Employee> myEmpls;
	private String tempVal;
	private Employee tempEmp;

	public XMLSaxProcessing() {
		myEmpls = new ArrayList<Employee>();
	}

	public void runExample() {
		parseDocument();
		printData();
	}

	private void parseDocument() {
		// factory instance
		SAXParserFactory spf = SAXParserFactory.newInstance();
		try {
			// SAX parser instance
			SAXParser sp = spf.newSAXParser();
			// parsing the file, and registration for the callback methods
			sp.parse("resources/employees.xml", this);
		} catch (SAXException se) {
			se.printStackTrace();
		} catch (ParserConfigurationException pce) {
			pce.printStackTrace();
		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}

	private void printData() {
		System.out.println("No of Employees '" + myEmpls.size() + "'.");
		Iterator<Employee> it = myEmpls.iterator();
		while (it.hasNext()) {
			System.out.println(it.next());
		}
	}

	// Event Handlers
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		tempVal = "";
		if (qName.equalsIgnoreCase("Employee")) {
			tempEmp = new Employee();
			tempEmp.setType(attributes.getValue("type"));
		}
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
		tempVal = new String(ch, start, length);
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		if (qName.equalsIgnoreCase("Employee")) {
			myEmpls.add(tempEmp);
		} else if (qName.equalsIgnoreCase("Name")) {
			tempEmp.setName(tempVal);
		} else if (qName.equalsIgnoreCase("Id")) {
			tempEmp.setId(Integer.parseInt(tempVal));
		} else if (qName.equalsIgnoreCase("Age")) {
			tempEmp.setAge(Integer.parseInt(tempVal));
		}
	}

	public static void main(String[] args) {
		XMLSaxProcessing spe = new XMLSaxProcessing();
		spe.runExample();
	}
}
```
## Futtatás
__Eredmény__

![Működés](gifs/domprocessing.gif)

> #### _kódmágia_
