package codeismagic.java8features.model;

public class Employee {
	private String name;
	private int id;
	private int age;
	private String type;
	
	public void setName(String name) {
		this.name = name;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public int getAge() {
		return age;
	}

	public String getType() {
		return type;
	}

	public Employee(String name, int id, int age, String type) {
		this.name=name;
		this.id=id;
		this.age=age;
		this.type=type;
	}
	
	public Employee() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Employee: [id:"+id+", name:"+name+", age:"+age+", type:"+type+"]";
	}

}
